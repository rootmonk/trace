package main

import (
	"errors"
	"fmt"

	"github.com/PratikDhanave/trace/tracelog"
)

func main() {
	//tracelog.StartFile(tracelog.LevelTrace, "/Users/bill/Temp/logs", 1)

	tracelog.Start(tracelog.LevelTrace)
	tracelog.Trace("main", "main", "Hello Trace")
	tracelog.Info("main", "main", "Hello Info")
	tracelog.Warning("main", "main", "Hello Warn")
	tracelog.Errorf(fmt.Errorf("Exception At..."), "main", "main", "Hello Error")

	Example()
	tracelog.Stop()
}

func foo() error {
	return errors.New("test")
}

func Example() {
	tracelog.Started("main", "Example")

	if err := foo(); err != nil {
		tracelog.CompletedError(err, "main", "Example")
		return
	}

	tracelog.Completed("main", "Example")
}
